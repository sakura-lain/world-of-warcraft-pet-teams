# My World of Warcraft pet teams

- My World of Warcraft pet teams, to be used with Rematch: [https://wow.curseforge.com/projects/rematch](https://www.curseforge.com/wow/addons/rematch)
<br/>Copy/paste teams in your Rematch import option.

- See my pet battles achievements: [https://worldofwarcraft.com/en-us/character/eu/temple-noir/Coatmor/achievements/pet-battles](https://worldofwarcraft.com/en-us/character/eu/temple-noir/Coatmor/achievements/pet-battles)
<br/>See my pets collection: [https://worldofwarcraft.com/en-us/character/eu/temple-noir/Coatmor/collections/pets](https://worldofwarcraft.com/en-us/character/eu/temple-noir/Coatmor/collections/pets)
